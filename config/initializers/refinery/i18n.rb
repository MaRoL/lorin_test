# encoding: utf-8
=begin
require 'refinerycms-core'
require 'routing-filter'

# lib/refinery/i18n.rb
module Refinery
  module I18n
    def self.locale_currencies
    end
  end
end

# lib/refinery/i18n/configuration.rb
module Refinery
  module I18n
    include ActiveSupport::Configurable

    config_accessor :locale_currencies, :default_frontend_currency

    # self.enabled = true
    # self.default_locale = :en
    # self.default_frontend_locale = self.default_locale
    # self.current_locale = self.default_locale
    # self.frontend_locales = [self.default_frontend_locale]
    # self.locales = self.built_in_locales
    self.locale_currencies = self.built_in_locale_currencies
  end
end

# lib/refinery/i18n.rb
module Refinery
  # autoload :I18nGenerator, 'generators/refinery/i18n_generator'

  module I18n
    class << Refinery::I18n
      attr_accessor :built_in_locale_currencies

      def built_in_locale_currencies
        @built_in_locale_currencies ||= {
                                          :"en-GB" => "£",
                                          :"en-US" => "US $",
                                          :"en-NZ" => "NZ $",
                                          :"en-AU" => "AU $",
                                          :"en-IE" => "IE €",
                                          :"fr-FR" => "FR €",
                                          # :"fr-BE" => "BE €",
                                          # :"fr-LU" => "LU €",
                                          # :"fr-CH" => "CHF",
                                          # :"nl-NL" => "NL €",
                                          :"nl-BE" => "BE €",
                                          :"da-DK" => "DK kr",
                                          :"no-NO" => "NO kr",
                                          :"fi-FI" => "FI kr", 
                                          :"es-ES" => "ES €",
                                          :"it-IT" => "IT €",
                                          # :"it-CH" => "CHF",
                                          :"de-DE" => "DE €",
                                          # :"de-LU" => "LU €",
                                          # :"de-CH" => "CHF",
                                          # :"de-AT" => "AT €",
                                          # :"sv-SE" => "kr",
                                          # :"pl-PL" => "zł",
                                          :"pt-PT" => "PT €"
                                        }
      end

      def current_frontend_currency
        if config.default_frontend_currency.present?
          config.default_frontend_currency
        else
          :"id-ID"
        end
      end
    end

    # require 'refinery/i18n/engine'
    # require 'refinery/i18n/configuration'
  end
end
=end

Refinery::I18n.configure do |config|
  config.enabled = true

  # config.default_locale = :en

  # config.current_locale = :en

  # config.default_frontend_locale = :en

  config.frontend_locales = [:en, :id]

  config.locales = {
                     :en=>"English",
                     :fr=>"Français",
                     :nl=>"Nederlands",
                     :"pt-BR"=>"Português",
                     :da=>"Dansk",
                     :nb=>"Norsk Bokmål",
                     :sl=>"Slovenian",
                     :es=>"Español",
                     :id=>"Indonesia",
                     :it=>"Italiano",
                     :de=>"Deutsch",
                     :lv=>"Latviski",
                     :ru=>"Русский",
                     :sv=>"Svenska",
                     :pl=>"Polski",
                     :"zh-CN"=>"Simplified Chinese",
                     :"zh-TW"=>"Traditional Chinese",
                     :el=>"Ελληνικά",
                     :rs=>"Srpski",
                     :cs=>"Česky",
                     :sk=>"Slovenský",
                     :ja=>"日本語",
                     :bg=>"Български"
                   }

=begin
  config.locale_currencies = {
                                :"en-GB" => "£",
                                :"en-US" => "US $",
                                :"en-NZ" => "NZ $",
                                :"en-AU" => "AU $",
                                :"en-IE" => "IE €",
                                :"fr-FR" => "FR €",
                                :"fr-BE" => "BE €",
                                :"fr-LU" => "LU €",
                                :"fr-CH" => "CHF",
                                :"nl-NL" => "NL €",
                                :"nl-BE" => "BE €",
                                :"da-DK" => "DK kr",
                                :"no-NO" => "NO kr",
                                :"fi-FI" => "FI kr", 
                                :"es-ES" => "ES €",
                                :"it-IT" => "IT €",
                                :"it-CH" => "CHF",
                                :"de-DE" => "DE €",
                                :"de-LU" => "LU €",
                                :"de-CH" => "CHF",
                                :"de-AT" => "AT €",
                                :"sv-SE" => "kr",
                                :"pl-PL" => "zł",
                                :"pt-PT" => "PT €"
                             }
=end
end
